import {StatusBar} from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, View, ScrollView, ActivityIndicator} from 'react-native';
import Header from "./components/Header";
import Formulario from "./components/Formulario";
import Cotizacion from "./components/Cotizacion";
import axios from 'axios';

export default function App() {

    const [moneda, setMoneda] = useState('');
    const [criptomoneda, setCriptomoneda] = useState('');
    const [consultarApi, setConsultarApi] = useState(false);
    const [restultado, setRestultado] = useState({});
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const cotizarCripto = async () => {
            setLoading(true);
            const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda},ETH&tsyms=${moneda}`;
            const result = await axios.get(url);
            setTimeout(() => {
                setConsultarApi(false)
                setLoading(false);
                setRestultado(result.data.DISPLAY[criptomoneda][moneda])
            })

        }
        if (consultarApi) {
            cotizarCripto();
        }
    }, [consultarApi]);

    return (
        <>
            <ScrollView>
                <Header/>

                <Image
                    style={styles.imagen}
                    source={require('./assets/img/cryptomonedas.png')}
                />
                <View style={styles.contenido}>
                    <Formulario
                        moneda={moneda}
                        criptomoneda={criptomoneda}
                        setMoneda={setMoneda}
                        setCriptomoneda={setCriptomoneda}
                        setConsultarApi={setConsultarApi}
                    />


                </View>
                <View style={{marginTop: 40}}>
                    {
                        loading ?
                            <ActivityIndicator size="large" color="#5e49e2"/> :
                            <Cotizacion resultado={restultado}/>

                    }
                </View>

            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    imagen: {
        width: '100%',
        height: 150,
        marginHorizontal: '2.5%'
    },
    contenido: {
        marginHorizontal: '2.5%'
    }

});

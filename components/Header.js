import React from 'react';
import {Text, StyleSheet, Platform} from "react-native";

const Header = (props) => {
    return (
        <>
            <Text style={styles.encabezado}>Criptomonedas</Text>
        </>
    )
};

const styles =  StyleSheet.create({
    encabezado: {
        paddingTop: Platform.OS === 'ios' ? 50 : 25,
        backgroundColor: '#5e49e2',
        paddingBottom: 10,
        textAlign: 'center',
        textTransform: 'uppercase',
        fontSize: 20,
        color: '#FFF',
        marginBottom: 30,
        // fontFamily: 'Lato-Black'
    }
})

export default Header;

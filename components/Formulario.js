import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {Text, View, StyleSheet, TouchableHighlight, Picker, Alert} from "react-native";

const Formulario = ({moneda, criptomoneda, setMoneda, setCriptomoneda, setConsultarApi}) => {
    const [selectedValue, setSelectedValue] = useState("java");

    const [criptomonedas, setCriptomonedas] = useState([]);

    useEffect(() => {
        const consultarAPI = async () => {
            const url = 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD';
            const result = await axios.get(url);
            setCriptomonedas(result.data.Data)
        }
        consultarAPI();
    }, []);


    const obtenerMoneda = coin => {
        setMoneda(coin)
    }

    const obtenerCripto = coin => {
        setCriptomoneda(coin)
    }

    const cotizarPrecio = () => {
        if (moneda.trim() === '' || criptomoneda.trim() === '') {
            mostrarAlert();
            return;
        }

        setConsultarApi(true)
    }

    const mostrarAlert = () => {
        Alert.alert(
            'Error...',
            'Ambos campos son Obligatorios',
            [
                {text: 'OK'}
            ]
        )
    }

    return (
        <View>
            <Text style={styles.label}>Moneda</Text>
            <Picker
                selectedValue={moneda}
                style={styles.selectPicker}
                onValueChange={(itemValue, itemIndex) => obtenerMoneda(itemValue)}
                itemStyle={{height: 120}}
            >
                <Picker.Item label="-  Seleccione  -" value="" />
                <Picker.Item label="Dolar de Estados Unidos" value="USD" />
                <Picker.Item label="Peso Mexicano" value="MXN" />
                <Picker.Item label="Euro" value="EUR" />
                <Picker.Item label="Libra Esterlina" value="GBP" />
            </Picker>

            <Text style={styles.label}>Criptomoneda</Text>
            <Picker
                selectedValue={criptomoneda}
                style={styles.selectPicker}
                onValueChange={(itemValue, itemIndex) => obtenerCripto(itemValue)}
                itemStyle={{height: 120}}
            >
                <Picker.Item label="-  Seleccione  -" value="" />
                {
                    criptomonedas.map(cripto => (
                        <Picker.Item key={cripto.CoinInfo.Id} label={cripto.CoinInfo.FullName} value={cripto.CoinInfo.Name} />
                    ))
                }

            </Picker>

            <TouchableHighlight
                style={styles.btnCotizar}
                onPress={() => cotizarPrecio()}
            >
                <Text style={styles.txtCotizar}>
                    Cotizar
                </Text>
            </TouchableHighlight>
        </View>
    )
};

const styles = StyleSheet.create({
    label: {
        fontSize: 22,
        marginVertical: 20,
        textTransform: 'uppercase'
    },
    btnCotizar: {
        backgroundColor: '#5e49e2',
        padding: 10,
        marginTop: 20
    },
    txtCotizar: {
        color: '#FFF',
        fontSize: 18,
        textTransform: 'uppercase',
        textAlign: 'center'
    }
})

export default Formulario;
